descargarSectores() {
    this.loadSectores().then(
      (res) => {
        this.sectoresData = res['tiposector'];

        for (let i = 0; i < this.sectoresData.length; i++) {
          this.pkidtiposector = this.sectoresData[i].pkidtiposector;
          this.codigotiposector = this.sectoresData[i].codigotiposector;
          this.nombretiposector = this.sectoresData[i].nombretiposector;
          this.tiposectoractivo = this.sectoresData[i].tiposectoractivo;
          this.creaciontiposector = this.sectoresData[i].creaciontiposector;
          this.modificaciontiposector = this.sectoresData[i].modificaciontiposector;
          this.descripciontiposector = this.sectoresData[i].descripciontiposector;

        }
        //console.log("PK: " + this.pkidtiposector + " COD: " + this.codigotiposector);
        this.setOcupado();

        //GUARDANDO
        let sector = {
          pkidtiposector: this.pkidtiposector,
          codigotiposector: this.codigotiposector,
          nombretiposector: this.nombretiposector,
          tiposectoractivo: this.tiposectoractivo,
          creaciontiposector: this.creaciontiposector,
          modificaciontiposector: this.modificaciontiposector,
          descripciontiposector: this.descripciontiposector
        }
        this.db.addSector(sector).then((res) => {
          console.log("Registro guardado desde API");
          this.setDesocupado();
        })

      },
      (error) => {
        console.error("Error al descargar sectores " + error);
      }
    )

  }




/*/*/*/ COD. DIEGO


   descargarSectores() {
    this.loadSectores().then(
      async (res) => {
        this.sectoresData = res['tiposector'];
        // this.events.publish('guardando', []);
        // this.setDesocupado();
        // this.setDesocupado();

        // this.setOcupado('guardando en bd...');
        console.log("INICIO DATOS GUARDADOS");

        let ContadorLongitud = this.sectoresData.length;
        console.log("Longitud: " + ContadorLongitud);

        let sectores = [];

        for (let i = 0; i < this.sectoresData.length; i++) {
           this.pkidtiposector = this.sectoresData[i].pkidtiposector;
          this.codigotiposector = this.sectoresData[i].codigotiposector;
          this.nombretiposector = this.sectoresData[i].nombretiposector;
           this.tiposectoractivo = this.sectoresData[i].tiposectoractivo;
           this.creaciontiposector = this.sectoresData[i].creaciontiposector;
           this.modificaciontiposector = this.sectoresData[i].modificaciontiposector;
           this.descripciontiposector = this.sectoresData[i].descripciontiposector;

           //GUARDANDO
           let sector = {
            pkidtiposector: this.pkidtiposector,
             codigotiposector: this.codigotiposector,
             nombretiposector: this.nombretiposector,
             tiposectoractivo: this.tiposectoractivo,
             creaciontiposector: this.creaciontiposector,
            modificaciontiposector: this.modificaciontiposector,
             descripciontiposector: this.descripciontiposector
           }

           /*let sector = {
             pkidtiposector: this.sectoresData[i].pkidtiposector,
             codigotiposector: this.sectoresData[i].codigotiposector,
            nombretiposector: this.sectoresData[i].nombretiposector,
             tiposectoractivo: this.sectoresData[i].tiposectoractivo,
            creaciontiposector: this.sectoresData[i].creaciontiposector,
             modificaciontiposector: this.sectoresData[i].modificaciontiposector,
            descripciontiposector: this.sectoresData[i].descripciontiposector
           };*/

          /*sectores.push({
            pkidtiposector: this.sectoresData[i].pkidtiposector,
            codigotiposector: this.sectoresData[i].codigotiposector,
            nombretiposector: this.sectoresData[i].nombretiposector,
            tiposectoractivo: this.sectoresData[i].tiposectoractivo,
            creaciontiposector: this.sectoresData[i].creaciontiposector,
            modificaciontiposector: this.sectoresData[i].modificaciontiposector,
            descripciontiposector: this.sectoresData[i].descripciontiposector
          });*/


        }

        try {          

          console.log("Longitud try: " + sectores.length);

          if ( await this.db.addSectores(sectores)) {
            console.log("Registro guardado desde API");
            console.log("- FIN DATOS GUARDADOS -");
          }
        } catch (err) {
          console.log(err.name);
          console.log(err.message);
          console.log(err.stack);
        }

      },
      (error) => {
        console.error("Error al descargar sectores " + error);
      }


    )

  }