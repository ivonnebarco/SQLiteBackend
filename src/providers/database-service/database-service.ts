import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/toPromise';
import { Events } from 'ionic-angular';

/*
  Generated class for the DatabaseServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseServiceProvider {

  db: SQLiteObject = null;

  constructor(public sqlite: SQLite, public events: Events) {
    console.log('Hello DatabaseServiceProvider Provider');
  }

  //Crea la base de datos si no existe y abre la conexión
  public openDb() {
    return this.sqlite.create({
      name: 'recaudoMovil.db',
      location: 'default' //el campo location es obligatorio
    })
      .then((db: SQLiteObject) => {
        this.db = db;
        console.log('bd creada2');
        this.events.publish('bdCreada', []);
      }).catch((error) => {
        console.log(error.name);
        console.log(error.message);
      });
  }

  //Creación de tablas
  public createTableSectores() {
    return this.db.executeSql("create table if not exists sectores" +
      "( pkidtiposector INTEGER PRIMARY KEY, codigotiposector INTEGER, nombretiposector TEXT, tiposectoractivo INTEGER, creaciontiposector TEXT, modificaciontiposector TEXT, descripciontiposector TEXT )", []);
  }

  //Guardar sectores
  public addSector(sector) {

    let sql = "INSERT INTO sectores (codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector) values (?,?,?,?,?,?)";
    return this.db.executeSql(sql, [sector.codigotiposector, sector.nombretiposector, sector.tiposectoractivo, sector.creaciontiposector, sector.modificaciontiposector, sector.descripciontiposector]);
  }

  public aux = 0;

  public async addSectores(sectores: any[]) {
    //this.aux = 0;
    //let i: number = 0;
    for (let sector of sectores) {
      let sql = "INSERT OR REPLACE INTO sectores (codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector) values (?,?,?,?,?,?)";
      return this.db.executeSql(sql, [sector.codigotiposector, sector.nombretiposector, sector.tiposectoractivo, sector.creaciontiposector, sector.modificaciontiposector, sector.descripciontiposector]);
    }

    /*sectores.forEach( sector => {

      let sql = "INSERT INTO sectores (codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector) values (?,?,?,?,?,?)";
      this.db.executeSql(sql, [sector.codigotiposector, sector.nombretiposector, sector.tiposectoractivo, sector.creaciontiposector, sector.modificaciontiposector, sector.descripciontiposector])
      .then(() => {
        this.aux ++;
        //console.log('Aux: '+this.aux)
      } )
      .catch((err) => {
        console.log(err.name);
          console.log(err.message);
          console.log(err.stack);
      });

    });*/
    //while (this.aux < sectores.length);
    return true;
    


  }

  //Listar sectores
  public getSectores() {
    let sql = "SELECT * FROM sectores";
    return this.db.executeSql(sql, []);
  }

  public modificaSector(sector) {
    let sql = "UPDATE sectores  SET codigotiposector = ?, nombretiposector = ?, tiposectoractivo = ?, creaciontiposector = ?, modificaciontiposector = ?, descripciontiposector = ? WHERE id = ? ";
    return this.db.executeSql(sql, [sector.codigotiposector, sector.nombretiposector, sector.tiposectoractivo, sector.creaciontiposector, sector.modificaciontiposector, sector.descripciontiposector, sector.id]);
  }

  public borrarSector(id) {
    let sql = "DELETE FROM sectores WHERE id= ? ";
    return this.db.executeSql(sql, [id]);
  }

}
